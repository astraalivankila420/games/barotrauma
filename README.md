## Setup

Mod collection: https://steamcommunity.com/sharedfiles/filedetails/?id=3231932920

Modilistan käyttö - Ilmeisesti vaan hostille pakollinen?

Kopioi asv_modlist.xml filu baron steami kansioihin:

Browse local files -> ModLists/

Tee kansio jos ei löydy. STEAMPATHS/Barotrauma/ModLists


## Neurotrauma

Lääkärinkäsikirja: https://trello.com/b/cabkctsx/neurotrauma-guide

TL;DR Ihminen on käänteinen sukellusvene, veren pitää pysyä _sisällä_. Sen jälkeen voi korjata muut vammat.

## Engineer