#!/bin/bash

# Used as entrypoint for Dockerfile

# Enable strict mode
set -euo pipefail
IFS=$'\n\t'

# Default values and environment variable validation
: "${GAMEDIR:?Must set GAMEDIR environment variable}"
: "${STEAMAPPID:?Must set STEAMAPPID environment variable}"
WORKSHOP_ID="602960"
TEMP_DIR="/tmp"
SERVER_CONFIG="/app/server/config_player.xml"

# Cleanup function
cleanup() {
    rm -f "${TEMP_DIR}/steam_"*.log
    rm -f "${TEMP_DIR}/mod_ids.txt"
    rm -f "${TEMP_DIR}/mods.xml"
}
trap cleanup EXIT

log() {
    echo "[SETUP] $*"
}

# Function to download a single mod
download_mod() {
    local MOD_ID="$1"
    local WORKSHOP_PATH="/app/Steam/steamapps/workshop/content/${WORKSHOP_ID}/${MOD_ID}"
    local STEAM_LOG="${TEMP_DIR}/steam_${MOD_ID}.log"

    log "Checking for existing mod at: $WORKSHOP_PATH"

    if [ -d "$WORKSHOP_PATH" ] &&
       [ -f "$WORKSHOP_PATH/filelist.xml" ] &&
       [ -s "$WORKSHOP_PATH/filelist.xml" ] &&
       xmllint --noout "$WORKSHOP_PATH/filelist.xml" 2>/dev/null; then
        log "Verified mod $MOD_ID is already installed correctly"
        return 0
    fi

    log "Starting download of mod $MOD_ID"

    # Create workshop directory structure
    mkdir -p "/app/Steam/steamapps/workshop/content/${WORKSHOP_ID}"

    if ! HOME=/app steamcmd +login anonymous +workshop_download_item "${WORKSHOP_ID}" "${MOD_ID}" +quit > "${STEAM_LOG}" 2>&1; then
        log "Failed to download mod $MOD_ID"
        log "SteamCMD output:"
        cat "${STEAM_LOG}"
        return 1
    fi

    if ! grep -q "Success" "${STEAM_LOG}"; then
        log "Mod $MOD_ID download may have failed"
        cat "${STEAM_LOG}"
        return 1
    fi

    if [ ! -f "${WORKSHOP_PATH}/filelist.xml" ]; then
        log "Download completed but filelist.xml is missing"
        return 1
    fi

    log "Successfully downloaded mod $MOD_ID"
    return 0
}

# Function to update server config with mods
update_config() {
    local MODS_XML="${TEMP_DIR}/mods.xml"
    local TEMP_FILE
    TEMP_FILE=$(mktemp)

    if [ ! -f "$MODS_XML" ]; then
        log "No mods XML file found at $MODS_XML"
        return 1
    fi

    # Extract mod IDs
    xmllint --xpath "//Workshop/@id" "$MODS_XML" 2>&1 | \
        grep -o '"[^"]*"' | \
        tr -d '"' > "${TEMP_DIR}/mod_ids.txt"

    # Update config with new mod entries
    awk -v mods_file="${TEMP_DIR}/mod_ids.txt" '
    /<regularpackages.*\/?>/ {
        print "        <regularpackages>"
        while ((getline mod_id < mods_file) > 0) {
            printf "            <package path=\"LocalMods/%s/filelist.xml\"/>\n", mod_id
        }
        print "        </regularpackages>"
        if ($0 !~ /\/>/) {
            while (getline > 0 && !/<\/regularpackages>/) { }
        }
        next
    }
    /<\/regularpackages>/ { next }
    { print $0 }
    ' "$SERVER_CONFIG" > "$TEMP_FILE"

    cp "$TEMP_FILE" "$SERVER_CONFIG"
}

# Function to install mods from URL
install_mods() {
    local MODS_URL="$1"
    local TEMP_XML="${TEMP_DIR}/mods.xml"

    log "Starting mod installation from URL: $MODS_URL"

    if ! curl -sSL "$MODS_URL" -o "$TEMP_XML"; then
        log "Failed to download mods list from URL"
        return 1
    fi

    log "Parsing mod list..."
    local MOD_IDS
    MOD_IDS=$(xmllint --xpath "//Workshop/@id" "$TEMP_XML" 2>&1 | grep -o '"[^"]*"' | tr -d '"')

    log "Found mod IDs: $MOD_IDS"

    local failed_mods=0
    for MOD_ID in $MOD_IDS; do
        log "Installing mod: $MOD_ID"
        if ! download_mod "$MOD_ID"; then
            log "Failed to install mod: $MOD_ID"
            ((failed_mods++))
        fi
    done

    if [ "$failed_mods" -gt 0 ]; then
        log "Warning: $failed_mods mod(s) failed to install"
    fi

    update_config
}

main() {
    # Initial setup
    log "Initializing SteamCMD"
    steamcmd +'quit'
    rm -rf /tmp/dumps

    # Update server
    log "Updating server"
    HOME=/app steamcmd @ShutdownOnFailedCommand @NoPromptForPassword \
        +force_install_dir "${GAMEDIR}" \
        +login anonymous \
        +app_update "${STEAMAPPID}" validate \
        +quit

    # Install mods if URL is provided
    if [ -n "${MODS_XML_URL:-}" ]; then
        log "Installing mods from ${MODS_XML_URL}"
        install_mods "${MODS_XML_URL}"
    fi

    log "Patching lua"
    curl -Lo patch.tar.gz https://github.com/evilfactory/LuaCsForBarotrauma/releases/download/latest/luacsforbarotrauma_patch_linux_server.tar.gz
    tar -zxf patch.tar.gz --overwrite -C "${GAMEDIR}"
    rm -f patch.tar.gz

    log "Copying config files"
    cp -f /app/setup/serversettings.xml /app/server
    cp -f /app/setup/clientpermissions.xml /app/server/Data

    log "Setting up LocalMods symlink"
    rm -rf /app/server/LocalMods
    ln -sf "/app/Steam/steamapps/workshop/content/${WORKSHOP_ID}" /app/server/LocalMods

    log "Starting server"
    exec "${GAMEDIR}/DedicatedServer"
}

main "$@"