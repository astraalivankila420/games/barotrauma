FROM opensuse/leap:15.6

# Add XML_URL build argument
ARG MODS_XML_URL

# Set environment variables
ENV STEAMAPPID=1026340 \
    GAMEDIR="/app/server" \
    WORKSHOPDIR="/app/Steam/steamapps/workshop" \
    HOME=/app

# Installation and setup
RUN set -eux \
    && mkdir -p /app \
    # Setup Microsoft repository
    && printf '%s\n' \
       '[packages-microsoft-com-prod]' \
       'name=packages-microsoft-com-prod' \
       'baseurl=https://packages.microsoft.com/opensuse/15/prod/' \
       'enabled=1' \
       'gpgcheck=1' \
       'gpgkey=https://packages.microsoft.com/keys/microsoft.asc' \
       > /etc/zypp/repos.d/microsoft-prod.repo \
    # Install packages
    && zypper --gpg-auto-import-keys \
        install --no-confirm --no-recommends --auto-agree-with-licenses \
           libicu \
           dotnet-runtime-6.0 \
           libSDL2-2_0-0 \
           steamcmd \
           curl \
           tar \
           xz \
           gzip \
           libxml2-tools \
    # Cleanup
    && zypper modifyrepo --disable --all \
    && steamcmd +'quit' \
    && rm -rf /var/cache/zypp/* \
            /root/Steam/appcache/httpcache/* \
            /tmp/*

# Setup Steam directories
RUN mkdir -p /app/.steam/sdk32 /app/.steam/sdk64 \
    && ln -s /usr/lib/steamcmd/linux32/steamclient.so /app/.steam/sdk32/steamclient.so \
    && ln -s /usr/lib/steamcmd/linux64/steamclient.so /app/.steam/sdk64/steamclient.so


VOLUME ["/app/server", "/app/setup", "/app/Steam/steamapps/workshop"]

WORKDIR $GAMEDIR

EXPOSE 27015/udp 27016/udp

CMD ["/bin/bash", "/app/setup/entrypoint.sh"]